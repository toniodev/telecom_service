
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_list_or_404, get_object_or_404, render
from django.urls import reverse
from datetime import datetime
from .models import Consumption, Product


def index(request):
    return render(
        request,
        'api/consumptions_list.html',
        {
            'consumptions': get_list_or_404(Consumption)
        }
    )


def consumption_details(request, id):
    return render(
        request,
        'api/consumption_details.html',
        {
            'consumption': get_object_or_404(Consumption, pk=id),
        }
    )


def consumption_form(request):
    return render(
        request,
        'api/consumption_form.html',
        {
            'products': get_list_or_404(Product)
        }
    )


def add_consumption(request):
    Consumption.objects.create(
        product=get_object_or_404(Product, pk=request.POST['product']), quantity=request.POST['quantity'], timestamp=datetime.now())
    return HttpResponseRedirect(reverse('index'))


def delete_consumption(request, id):
    get_object_or_404(Consumption, pk=id).delete()
    return HttpResponseRedirect(reverse('index'))
