from django.contrib import admin

from .models import ProductsCategory, Product, Consumption

admin.site.register(ProductsCategory)
admin.site.register(Product)
admin.site.register(Consumption)
