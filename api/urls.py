from django.urls import path
from api import views

urlpatterns = [
    path("", views.index, name="index"),
    path('consumptions/form', views.consumption_form, name='consumption_form'),
    path('consumptions/add', views.add_consumption, name='add_consumption'),
    path("consumptions/<id>", views.consumption_details,
         name="consumption_details"),
    path("consumptions/delete/<id>", views.delete_consumption,
         name="delete_consumption"),
]
