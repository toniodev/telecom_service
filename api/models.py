from django.db import models

class ProductsCategory(models.Model):
    code = models.CharField(max_length=2, primary_key=True)
    name = models.CharField(max_length=100)

class Product(models.Model):
    code = models.CharField(max_length=5, primary_key=True)
    name = models.CharField(max_length=100)
    category = models.ForeignKey(ProductsCategory, on_delete=models.PROTECT)

class Consumption(models.Model):
    timestamp = models.DateTimeField()
    product =  models.ForeignKey(Product, on_delete=models.PROTECT)
    quantity = models.IntegerField()